# README #

Implementar um componente com Cards contendo (em cada card) imagem, título e descrição. A organização do componente deve utilizar flexbox.


Material de auxílio:


Vídeos:
https://www.youtube.com/watch?v=WKHB1UOi-i4

https://www.youtube.com/watch?v=WBMN-PcwqG8

https://www.youtube.com/watch?v=0OWlr3D3wFA

https://www.youtube.com/watch?v=x-4z_u8LcGc



Artigos:
https://css-tricks.com/snippets/css/a-guide-to-flexbox/

https://www.alura.com.br/artigos/css-guia-do-flexbox



Aula para dúvidas sobre a utilização do flexbox 14/09/2021.


Entrega em grupo. 
Um dos grupos será sorteado no dia 21 de Setembro para apresentar como foi utilizado o Flex-Box dentro de sua aplicação.
Observação: fazer dentro de um projeto Angular. 
A entrega é do link do repositório do projeto.